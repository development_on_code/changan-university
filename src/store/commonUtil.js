/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-05-10 14:36:31
 * @LastEditors: LiLei
 * @LastEditTime: 2021-05-12 14:22:19
 */
// 引入配置项
/* eslint-disable */
//不删除的缓存
/* eslint-disable */
let times = null
function twoDigits (val) {
    if (val < 10) return "0" + val;
    return val;
}
const commonUtil = {
    state: {
        activeTime: '',
        dictionaries: [],
        storageName: "vue-vite",
    },
    getters: {
        //获取当前时间
        getActiveTime: state => state.activeTime,
        //是移动端返回true,否则false
        isMobile: state => {
            return (/phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone|webOS|android/i.test(navigator.userAgent))
        },
        /**
         * @description 获取state的值
         * @param
         * @returns Object
         */
        getState: state => state,
        /**
         * @desc 获取storage中存储的值
         * @param {Object} key
         */
        getItem: state => key => {
            if (!key) {
                return;
            }
            const _storageName = JSON.parse(
                window.localStorage.getItem(state.storageName) || "{}"
            );
            return _storageName[key] || "";
        },
        /**
         * @desc 获取sessionStorage中存储的值
         * @param {Object} key
         */
        getSessionItem: state => key => {
            if (!key) {
                return;
            }
            const _storageName = JSON.parse(
                window.sessionStorage.getItem(state.storageName) || "{}"
            );
            return _storageName[key] || "";
        },
        /**
        * @desc 将时间戳转换成正常时间格式
        * @param {Object} key
        */
        timestampToTime: state => timestamp => {
            if (!timestamp) {
                return "——"
            }
            var date = new Date(timestamp);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
            var Y = date.getFullYear() + '-';
            var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            var D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
            var h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
            var m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
            var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
            return Y + M + D + h + m + s;
        }
    },
    mutations: {
        //获取时间
        getTime (state) {
            var timezone = 8;
            var offset_GMT = new Date().getTimezoneOffset();
            var nowDate = new Date().getTime();
            var today = new Date(nowDate + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000);
            var date = today.getFullYear() + "年" + twoDigits(today.getMonth() + 1) + "月" + twoDigits(today.getDate()) + "日";
            var time = twoDigits(today.getHours()) + ":" + twoDigits(today.getMinutes()) + ":" + twoDigits(today.getSeconds());
            state.activeTime = date + '  ' + time;
        },
        /**
         *
         * @param {*} state
         * @param {*} opts
        */
        realTimeGetActiveTime (state) {
            this.commit("getTime");
            times = setInterval(() => {
                this.commit("getTime");
            }, 1000);
        },
        /**
         * @name 保存
         * @param {key} opts 存储的名称
         * @param {value} opts 需要存储的值
         */
        saveItem (state, opts) {
            if (!opts.key) {
                return;
            }
            const _storageName = JSON.parse(
                window.localStorage.getItem(state.storageName) || "{}"
            );
            _storageName[opts.key] = opts.value;
            window.localStorage.setItem(
                state.storageName,
                JSON.stringify(_storageName)
            );
        },
        /**
         * @name 清楚缓存
         * @param {key} key 存储的名称
         */
        clearItem (state, key) {
            if (!key) {
                return;
            }
            const _storageName = JSON.parse(
                window.localStorage.getItem(state.storageName) || "{}"
            );
            if (_storageName[key]) {
                delete _storageName[key];
            }
            window.localStorage.setItem(
                state.storageName,
                JSON.stringify(_storageName)
            );
        },
        /**
         * @name 保存 sessionStorage
         * @param {key} opts 存储的名称
         * @param {value} opts 需要存储的值
         */
        saveSessionItem (state, opts) {
            if (!opts.key) {
                return;
            }
            const _storageName = JSON.parse(
                window.sessionStorage.getItem(state.storageName) || "{}"
            );
            _storageName[opts.key] = opts.value;
            window.sessionStorage.setItem(
                state.storageName,
                JSON.stringify(_storageName)
            );
        },
        /**
         * @name 清楚缓存 sessionStorage
         * @param {key} key 存储的名称
         */
        clearSessionItem (state, key) {
            if (!key) {
                return;
            }
            const _storageName = JSON.parse(
                window.sessionStorage.getItem(state.storageName) || "{}"
            );
            if (_storageName[key]) {
                delete _storageName[key];
            }
            window.sessionStorage.setItem(
                state.storageName,
                JSON.stringify(_storageName)
            );
        }
    },
    actions: {

    }
};
export default commonUtil;
