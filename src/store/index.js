/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2020-12-01 15:48:16
 * @LastEditors: LiLei
 * @LastEditTime: 2022-02-15 17:10:55
 */
import { createStore } from "vuex";
import commonUtil from "./commonUtil";

export default createStore({
    state: {
        isLoadding: false, // 是否显示loadding
    },
    getters: {
        getIsLoadding: (state) => state.isLoadding,
    },
    mutations: {
        setLoadding(state, flag) {
            state.isLoadding = flag;
        },
    },
    modules: {
        commonUtil,
    },
});
