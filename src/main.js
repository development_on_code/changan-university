/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:01:19
 * @LastEditors: LiLei
 * @LastEditTime: 2022-02-15 15:28:07
 */
import { createApp } from "vue";
import Antd from "ant-design-vue";
import App from "./App.vue";
import "ant-design-vue/dist/antd.css";
import router from "./router";
import store from "./store";

const app = createApp(App);
app.use(router).use(store).use(Antd).mount("#app");
app.config.productionTip = false;
