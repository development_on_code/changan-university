/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:17:40
 * @LastEditors: LiLei
 * @LastEditTime: 2022-02-14 18:26:43
 */
import {
    createRouter,
    createWebHashHistory,
    createWebHistory,
} from "vue-router";
const routes = [
    {
        path: "/",
        redirect: "/index",
    },
    {
        path: "/",
        name: "Main",
        component: () => import(/* webpackChunkName: 'Main' */ "../Main.vue"),
        children: [
            {
                path: "/index",
                name: "index",
                component: () =>
                    import(
                        /* webpackChunkName: 'index' */ "/@/pages/index/index.vue"
                    ),
            },
            {
                path: "/search",
                name: "search",
                component: () =>
                    import(
                        /* webpackChunkName: 'search' */ "/@/pages/search/index.vue"
                    ),
            },
        ],
    },
];

const router = createRouter({
    history:
        process.env.NODE_ENV === "development"
            ? createWebHistory()
            : createWebHashHistory(),
    routes,
});
export default router;
