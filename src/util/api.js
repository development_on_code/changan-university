/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-26 12:05:38
 * @LastEditors: LiLei
 * @LastEditTime: 2022-02-16 21:27:54
 */
import service from "./request";
// 获取 接入数据
export function searchList(opts) {
    let obj = opts || {};
    return service.request({
        isForm: true,
        method: "get",
        url:
            import.meta.env.VITE_URl +
            "citylife/citylife/nocheck/getTongxinList",
        data: obj.data || {},
        isLoadding: obj.isLoadding || false,
        isCloseLoadding: obj.isCloseLoadding || false,
        isPop: obj.isPop,
    });
}
