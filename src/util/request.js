/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2022-02-15 17:09:34
 * @LastEditors: LiLei
 * @LastEditTime: 2022-02-17 20:17:56
 */
import axios from "axios";
import store from "../store";
// import generate from "./safetyCertificate.js";
import qs from "qs";
import { message } from "ant-design-vue";
function getCookie(name) {
    var prefix = name + "=";
    var start = document.cookie.indexOf(prefix);

    if (start == -1) {
        return null;
    }

    var end = document.cookie.indexOf(";", start + prefix.length);
    if (end == -1) {
        end = document.cookie.length;
    }

    var value = document.cookie.substring(start + prefix.length, end);
    return unescape(value);
}
// 创建axios
const service = axios.create({
    withCredentials: true,
    contentType: false,
    processData: false,
    crossDomain: true,
    headers: {
        // Authorization: `Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6Ijg5MGI0M2RjLTI5OWEtNDkyZC1iOTFiLWViMmI5Yjk1ZDk3MCJ9.Q66Im4VLBhqr-TCwDRWntfTq9td8D6uEfS79tWgDflgaFUUNp4GMxLppFtOkffUyFo5ZXJshkikMuxDAZkTm2A`,
        // "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Content-Type": "application/json",
        // "content-type": "appliaction/x-www-form-urlencoded",
        // "set-cookie":
        //     getCookie("dev_nicai_auth_session_id") ||
        //     "32fed9fa-3f5e-4507-a9ee-887630a19126",
    },
    // baseURL: "http://localhost:8000/",
    // baseURL:
    //     process.env.NODE_ENV === "development"
    //         ? "http://localhost:8000/"
    //         : "https://dev-mpcs.dazzlesky.com/",
    baseURL: "",
    timeout: 1000 * 40,
});

// 请求时间
// 添加请求拦截器
service.interceptors.request.use(
    function (config) {
        // 在发送请求之前做些什么
        // console.log("config", config);
        // 开启loadding
        // 是否开启loadding，默认开启
        var isLoadding =
            config.isLoadding === undefined ? true : config.isLoadding;
        if (isLoadding) {
            store.commit("setLoadding", isLoadding);
        }
        // 判断是不是form表单请求
        if (config.isForm) {
            const datas = qs.stringify(config.data, {
                arrayFormat: "indices",
                allowDots: true,
            });
            let formData = new FormData();
            for (let key in datas) {
                formData.append(key, datas[key]);
            }
            if (datas) {
                config.url = config.url + "?" + datas;
                console.log("datas", datas, config);
            }

            // config.headers["Content-Type"] =
            //     "application/x-www-form-urlencoded; charset=UTF-8";
            config.data = datas;
        } else {
            config.data = config.data;
        }

        return config;
    },
    function (error) {
        // 对请求错误做些什么
        return Promise.reject(error);
    }
);

// 添加响应拦截器
service.interceptors.response.use(
    function (response, aa) {
        const config = response.config;

        //是否关闭loadding
        var isCloseLoadding =
            config.isCloseLoadding === undefined
                ? true
                : config.isCloseLoadding;
        // 是否开始错误弹框，默认开启
        var isPop = config.isPop === undefined ? true : config.isPop;

        if (isCloseLoadding) {
            store.commit("setLoadding", false);
        }
        // 对响应数据做点什么
        if (response.status === 200) {
            const data = response.data;
            if (data.code == 200) {
                const result = data.data || data.result || data.rows || {};
                return typeof result === "string" ? JSON.parse(result) : result;
            } else {
                if (isPop) {
                    setTimeout(() => {
                        message.error(data.message || "请求异常");
                    }, 400);
                }
                return Promise.reject(data);
            }
        }
        return response;
    },
    function (error, a) {
        // 对响应错误做点什么
        store.commit("setLoadding", false);
        setTimeout(() => {
            message.error("请求异常");
        }, 400);
        return Promise.reject(error);
    }
);

export default service;
