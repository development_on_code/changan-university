/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:01:19
 * @LastEditors: LiLei
 * @LastEditTime: 2022-02-17 21:49:54
 */
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

const { resolve } = require("path");
// import styleImport from "vite-plugin-style-import";
const plugins = [
    vue(),
    // styleImport({
    //     libs: [
    //         {
    //             libraryName: "ant-design-vue",
    //             esModule: true,
    //             resolveStyle: (name) => `ant-design-vue/lib/${name}/style`,
    //         },
    //     ],
    // }),
];
export default defineConfig({
    presets: ["transform-remove-console", "@vue/cli-plugin-babel/preset"],
    plugins,
    // // 去除打印
    // configureWebpack: (config) => {
    //     if (process.env.NODE_ENV === "production") {
    //         config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true;
    //     }
    // },
    build: {
        // cssCodeSplit: true,
        rollupOptions: {
            output: {
                chunkFileNames: "js/[name].[hash].js",
                entryFileNames: "js/[name].[hash].js",
                // 分割打包
                // manualChunks(id) {
                //     if (id.includes("node_modules")) {
                //         return id
                //             .toString()
                //             .split("node_modules/")[1]
                //             .split("/")[0]
                //             .toString();
                //     }
                // },
            },
        },
        assetsDir: "assets",
        publicPath: "./", // 公共路径
        outDir: "dist",
    },

    base: "./", // 公共路径
    server: {
        host: "0.0.0.0", // ← 新增内容 ←
    },

    productionSourceMap: false, // 生产环境下css 分离文件
    css: {
        // 引用全局 scss
        preprocessorOptions: {
            scss: {
                additionalData: '@import "./src/module.scss";',
            },
        },
    },
    resolve: {
        //别名
        alias: {
            "/@": resolve(__dirname, "./src"), //注意 后面/不需要了
        },
    },
});
